import argparse
import sys
from pathlib import Path
from typing import List, Set
import csv


class Monster:
    def __init__(
        self,
        id: str,
        name: str,
        types: List[str],
        weaknesses: List[str],
        evolution: List[str],
    ):
        self.id = id
        self.name = name
        self.types = types
        self.weaknesses = weaknesses
        self.evolution = evolution


class MonsterRegistry:
    def __init__(self):
        """Initialize the Monster Registry."""

        # Associate monster name with monster object
        self.monster_registry_dict: dict = {}

        # Provide a quick dict to look up monsters that are certain type
        self.monster_type_dict: dict = {}

        # Associate type : monster/s that are weak against it
        self.monster_weakness: dict = {}

    def parse_monster_csv(self, monster_registry_file: Path) -> None:
        """Parse the monster csv file and update the object variables.

        Args:
            monster_registry_file (Path): the file path for the monster csv

        """
        # open file with a with to ensure we close it after reading everything we want
        with open(monster_registry_file, newline="\n") as csv_file:
            reader = csv.reader(csv_file)
            header = next(reader, None)  # read the header line

            # header fields can be dynamic
            for index, column_header in enumerate(header):
                if column_header == "Name":
                    name_index = index
                elif column_header == "Evolution":
                    evolution_index = index
                elif column_header == "Types":
                    types_index = index
                elif column_header == "ID":
                    id_index = index
                elif column_header == "Weaknesses":
                    weakness_index = index

            # Unpack the monster attributes
            for monster_row in reader:
                monster_id = monster_row[id_index]
                monster_name = monster_row[name_index]
                monster_types = monster_row[types_index]
                monster_weaknesses = monster_row[weakness_index]
                monster_evolution = monster_row[evolution_index]

                # Convert the strings to list using the comma as a delimiter
                if monster_types:
                    monster_types = monster_types.split(",")
                # incase there is no value provided
                else:
                    monster_types = []

                if monster_weaknesses:
                    monster_weaknesses = monster_weaknesses.split(",")
                else:
                    monster_weaknesses = []

                if monster_evolution:
                    monster_evolution = monster_evolution.split(",")
                else:
                    monster_evolution = []

                # Add the Monster object to the registry dict using the name as the key
                self.monster_registry_dict[monster_name.lower()] = Monster(
                    id=monster_id,
                    name=monster_name,
                    types=monster_types,
                    weaknesses=monster_weaknesses,
                    evolution=monster_evolution,
                )

                # Also use the monster_id as a key (used by dfs)
                # This could be improved, but doesn't hurt much since we reuse the monster object to reduce memory usage
                self.monster_registry_dict[monster_id] = self.monster_registry_dict[monster_name.lower()]

                # Loop through the monster's type/s
                for type in monster_types:

                    # this will allow for an O(1) lookup monster with a specified type
                    # Update the dict to include the monster name into the monster type dict
                    if type in self.monster_type_dict:
                        self.monster_type_dict[type].append((monster_id, monster_name))
                    else:
                        self.monster_type_dict[type] = [(monster_id, monster_name)]

                # Loop through the monster's weakness/es
                for weakness in monster_weaknesses:

                    # this will allow for an O(1) lookup for monster's weak to a specified type
                    # Update the dict to include the monster name into the monster weakness dict
                    if weakness in self.monster_weakness:
                        self.monster_weakness[weakness].append(
                            (monster_id, monster_name)
                        )
                    else:
                        self.monster_weakness[weakness] = [(monster_id, monster_name)]

        # If this script were to provide multiple calls to get monster stats we could call get_monster_evolution on all
        # monsters initially so lookup could be O(1) in the future

    def get_monsters_weak_against(self, monster_weak_types: List[str]) -> Set:
        """Get the monsters that this monster is weak against.

        Args:
            monster_weak_types (List[str]): the list of weak types for this monster

        Returns:
            (set): the list of monsters that this monster is strong against

        """
        # use a set to ensure to no duplicate monsters in this list
        weak_against_monsters = set()

        for monster_type in monster_weak_types:

            # check if any monster has the type at all
            if monster_type in self.monster_type_dict:

                # Update the set to include the monsters
                weak_against_monsters.update(self.monster_type_dict[monster_type])

        # If there is no weak against monster
        if len(weak_against_monsters) == 0:
            return {("0", "None")}

        # return the list of monsters weak to the specified types - sorted by monster_id (first index in tuple)
        return sorted(weak_against_monsters)

    def get_monsters_strong_against(self, monster_type_list: List[str]) -> Set:
        """Get the monsters that this monster is strong against.

        Args:
            monster_type_list (List[str]): the list of types for this monster

        Returns:
            (set): the list of monsters that this monster is strong against

        """
        # use a set to ensure we only include the strong against monster once
        strong_against_monsters = set()

        for monster_type in monster_type_list:

            # check if any monster has the weakness at all
            if monster_type in self.monster_weakness:

                # Update the set to include
                strong_against_monsters.update(self.monster_weakness[monster_type])

        # If there is no strong against monster
        if len(strong_against_monsters) == 0:
            return {("0", "None")}

        # return the list of monsters weak to the specified types - sorted by monster_id (first index in tuple)
        return sorted(strong_against_monsters)

    def dfs(self, visited: set, monster_id: str, possible_evolution_list: List) -> None:
        """Recursively travel down all monster evolutions.

        Args:
            visited (set): list of monsters already visited
            monster_id (str): monster identifier
            possible_evolution_list (List): list of possible evolution trees

        """
        # update the visited list with the current monster id
        visited.add(monster_id)

        # Loop through possible evolutions for this monster
        for possible_evolution in self.monster_registry_dict[monster_id].evolution:

            # check if this evolution has been visited
            if possible_evolution not in visited:

                # recursive call to check this evolution
                self.dfs(visited.copy(), possible_evolution, possible_evolution_list)

        # add all possible evolution paths
        possible_evolution_list.append(visited)

    def get_monster_evolution(self, monster: Monster) -> List:
        """Retrieve all evolution paths for this monster.

        Args:
            monster (Monster): the monster object

        """
        # keep visited paths in a set to ignore duplicates
        visited = set()
        possible_evolution_list = []

        # Initial call to dfs recurisve method
        self.dfs(visited, monster.id, possible_evolution_list)

        # remove evolution subsets to get the full path, this could be handled by the dfs method to ignore them
        for x_index, _ in enumerate(list(possible_evolution_list)):
            for element_y in list(possible_evolution_list[x_index + 1 :]):

                # check if either element_x or element_Y is a subset of each other
                if possible_evolution_list[x_index].issubset(element_y):
                    possible_evolution_list.remove(possible_evolution_list[x_index])
                elif element_y.issubset(possible_evolution_list[x_index]):
                    possible_evolution_list.remove(element_y)

        return possible_evolution_list

    def print_monster_stats(self, monster: str):
        """Print all monster stats to the stdout.

        Args:
            monster (Monster): the monster object


        """
        monster_object = self.monster_registry_dict[monster.lower()]
        space_indent = " " * 4

        print("ID:")
        print(f"{space_indent}{monster_object.id}")

        print("Strong against:")

        # loop through the strong against monsters and ignore the monster_id
        for _, monster in self.get_monsters_strong_against(
            monster_type_list=monster_object.types
        ):
            print(f"{space_indent}{monster}")

        # loop through the weak against monsters and ignore the monster_id
        print("Weak against:")
        for _, monster in self.get_monsters_weak_against(
            monster_weak_types=monster_object.weaknesses
        ):
            print(f"{space_indent}{monster}")

        # get evolution trees
        monster_evolution_trees = self.get_monster_evolution(monster=monster_object)
        print("Evolution:")

        # if no evolution tree possible
        if len(monster_evolution_trees) == 0:
            print(f"{space_indent}None")

        for monster_evolution in monster_evolution_trees:
            output_tree = ""
            for monster in sorted(monster_evolution):
                output_tree += f"{self.monster_registry_dict[monster].name} > "
            print(f"{space_indent}{output_tree[:-3]}")


def main():
    parser = argparse.ArgumentParser(description="Print a Monster registry report.")

    parser.add_argument(
        "file_dest",
        type=str,
    )
    parser.add_argument(
        "monster",
        type=str,
    )

    args = parser.parse_args()

    # Create Monster Registry object
    monster_registry = MonsterRegistry()

    monster_file_path = Path(args.file_dest)

    # verify file existence and exit 1 if it doesn't
    if not monster_file_path.exists():
        sys.exit(1)

    # parse csv
    monster_registry.parse_monster_csv(monster_registry_file=monster_file_path)

    # print monster stats
    monster_registry.print_monster_stats(monster=args.monster)


if __name__ == "__main__":
    main()
